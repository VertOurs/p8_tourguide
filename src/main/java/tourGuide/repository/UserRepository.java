package tourGuide.repository;

import tourGuide.model.User;

import java.util.List;

public interface UserRepository {

    User getUser(String userName);
    List<User> getAllUsers();
    User saveUser(User user);
    void initializeInternalUsers();
}
