package tourGuide.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import tourGuide.dto.AttractionDTO;
import tourGuide.dto.UserPreferencesDTO;
import tourGuide.service.TourGuideService;
import tourGuide.model.User;
import tourGuide.model.UserReward;
import tripPricer.Provider;

@RestController
@Slf4j
public class TourGuideController {

	private final TourGuideService service;

    public TourGuideController(TourGuideService service) {
        this.service = service;
        Locale.setDefault(Locale.US);
    }

    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }
    
    @RequestMapping("/getLocation")
    public Location getLocation(@RequestParam String userName) {
        User user = service.getUser(userName);
    	VisitedLocation visitedLocation = service.getUserLocation(user);
        return visitedLocation.location;
    }

    @RequestMapping("/getNearbyAttractions")
    public List<AttractionDTO> getNearbyAttractions(@RequestParam String userName) {
        User user = service.getUser(userName);
        return service.getFiveMostNearestAttractions(user);
    }
    
    @RequestMapping("/getRewards")
    public List<UserReward> getRewards(@RequestParam String userName) {
        User user = service.getUser(userName);
        return service.getUserRewards(user);
    }
    
    @RequestMapping("/getAllCurrentLocations")
    public HashMap<UUID, Location> getAllCurrentLocations() {
        HashMap<UUID, Location> locations = service.getAllCurrentLocations();
    	return locations;
    }
    
    @RequestMapping("/getTripDeals")
    public List<Provider> getTripDeals(@RequestParam String userName) {
        User user = service.getUser(userName);
    	List<Provider> providers = service.getTripDeals(user);
    	return providers;
    }

    @PutMapping("/updateUserPreferences")
    public String updateUserPreferences(@RequestParam String userName, @RequestBody UserPreferencesDTO userPreferencesDto) {
        service.updateUserPreferences(userName, userPreferencesDto);
        return " the user : " + userName + ", is updated !";
    }
}