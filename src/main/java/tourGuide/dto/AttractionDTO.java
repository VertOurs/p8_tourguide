package tourGuide.dto;

import gpsUtil.location.Location;
import lombok.Data;

@Data
public class AttractionDTO {

    private final String attractionName;
    private final Location attractionLocation;
    private final Location userLocation;
    private final double distance;
    private final int rewardPoint;
}
