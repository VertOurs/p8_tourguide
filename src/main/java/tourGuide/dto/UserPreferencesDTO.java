package tourGuide.dto;

import lombok.Data;

@Data
public class UserPreferencesDTO {

    private final int tripDuration;
    private final int childrenNumber;
    private final int adultsNumber;
}
