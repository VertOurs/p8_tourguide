package tourGuide.service;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import gpsUtil.location.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import tourGuide.dto.AttractionDTO;
import tourGuide.dto.UserPreferencesDTO;
import tourGuide.repository.UserRepository;
import tourGuide.tracker.Tracker;
import tourGuide.model.User;
import tourGuide.model.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

import static tourGuide.repository.UserRepositoryStub.tripPricerApiKey;

@Service
@Slf4j
public class TourGuideService {

	private final UserRepository repository;
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();

	public Tracker tracker;
	private ExecutorService executorService = Executors.newFixedThreadPool(200);

	boolean testMode = true;
	
	public TourGuideService(UserRepository repository, GpsUtil gpsUtil, RewardsService rewardsService) {
		this.repository = repository;
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;

			if(testMode) {
				log.info("TestMode enabled");
				log.debug("Initializing users");
				repository.initializeInternalUsers();
				log.debug("Finished initializing users");
			}
		tracker = new Tracker(this);
		addShutDownHook();
	}
	
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}
	
	public VisitedLocation getUserLocation(User user) {
		VisitedLocation userLocation;

		if (user.getVisitedLocations().isEmpty()) {
				userLocation = trackUserLocationSync(user);
		} else {
			userLocation = user.getLastVisitedLocation();
		}
		return userLocation;
	}

	public void trackUserLocation(User user) {

		 CompletableFuture.supplyAsync(() -> {
			 VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
			 user.addToVisitedLocations(visitedLocation);
			 rewardsService.calculateRewards(user);
			 return visitedLocation;
			 }, executorService);

	}

	public VisitedLocation trackUserLocationSync(User user) {
		VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
		user.addToVisitedLocations(visitedLocation);
		rewardsService.calculateRewards(user);
		return visitedLocation;
	}
	
	public List<Provider> getTripDeals(User user) {
		int cumulativeRewardPoints =
				user.getUserRewards().stream()
						.mapToInt(i -> i.getRewardPoints())
						.sum();

		List<Provider> providers =
				tripPricer.getPrice(
						tripPricerApiKey,
						user.getUserId(),
						user.getUserPreferences().getNumberOfAdults(),
						user.getUserPreferences().getNumberOfChildren(),
						user.getUserPreferences().getTripDuration(),
						cumulativeRewardPoints
				);

		user.setTripDeals(providers);
		return providers;
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}

	public User getUser(String userName) {
		return repository.getUser(userName);
	}

	public List<User> getAllUsers() {
		return repository.getAllUsers();
	}

	public void addUser(User user2) {
		repository.saveUser(user2);
	}

	public HashMap<UUID, Location> getAllCurrentLocations() {
		List<User> users = getAllUsers();
		HashMap<UUID, Location> locationMap = new HashMap<>();

		for ( User user : users) {
			locationMap.put(
					user.getUserId(),
					user.getLastVisitedLocation().location
			);
		}
		return locationMap;
	}

	public List<AttractionDTO> getFiveMostNearestAttractions(User user)  {
		List<Attraction> allAttractions = gpsUtil.getAttractions();
		List<AttractionDTO> attractionDTOs = new ArrayList<>();

		Location userLocation = getUserLocation(user).location;

		for (Attraction attraction : allAttractions) {
			Location attractionLocation = new Location(attraction.latitude, attraction.longitude);
			double distance = rewardsService.getDistance(attractionLocation, userLocation);
			int reward = rewardsService.getRewardPoints(attraction, user);

			attractionDTOs.add(
					new AttractionDTO(
							attraction.attractionName,
							attractionLocation,
							userLocation,
							distance,
							reward
							));
		}
		return getAttractionDTOsSorted(attractionDTOs);
	}

	private List<AttractionDTO> getAttractionDTOsSorted(List<AttractionDTO> attractionDTOs) {
		List<AttractionDTO> sortedList =
				attractionDTOs.stream()
						.sorted(Comparator
								.comparingDouble(AttractionDTO::getDistance))
						.collect(Collectors.toList());

		if (sortedList.size() > 5) {
			return sortedList.subList(0, 5);
		} else {
			return sortedList;
		}
	}

    public User updateUserPreferences(String userName, UserPreferencesDTO dto) {
		User user = repository.getUser(userName);
		user.getUserPreferences().setTripDuration(dto.getTripDuration());
		user.getUserPreferences().setNumberOfChildren(dto.getChildrenNumber());
		user.getUserPreferences().setNumberOfAdults(dto.getAdultsNumber());
		return repository.saveUser(user);
    }
}
