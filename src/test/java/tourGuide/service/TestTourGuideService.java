package tourGuide.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.dto.AttractionDTO;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.UserRepository;
import tourGuide.repository.UserRepositoryStub;
import tourGuide.model.User;
import tripPricer.Provider;

public class TestTourGuideService {

	UserRepository repository = new UserRepositoryStub();
	GpsUtil gpsUtil = new GpsUtil();
	RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
	TourGuideService tourGuideService = new TourGuideService(repository, gpsUtil, rewardsService);

	User user1 = new User(
			UUID.randomUUID(),
			"jon",
			"000",
			"jon@tourGuide.com");
	User user2 = new User(
			UUID.randomUUID(),
			"jon2",
			"000",
			"jon2@tourGuide.com");


	@Before
	public void setUp() {
		tourGuideService.addUser(user1);
		tourGuideService.addUser(user2);
	}

	@Test
	public void getUserLocation() {
		Locale.setDefault(Locale.US);

		VisitedLocation visitedLocation = tourGuideService.trackUserLocationSync(user1);
		tourGuideService.tracker.stopTracking();

		assertEquals(visitedLocation.userId, user1.getUserId());
	}

	@Test
	public void addUser() {
		User retrivedUser = tourGuideService.getUser(user1.getUserName());
		User retrivedUser2 = tourGuideService.getUser(user2.getUserName());
		tourGuideService.tracker.stopTracking();

		assertEquals(user1, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}

	@Test
	public void getAllUsers() {
		List<User> allUsers = tourGuideService.getAllUsers();
		tourGuideService.tracker.stopTracking();

		assertTrue(allUsers.contains(user1));
		assertTrue(allUsers.contains(user2));
	}

	@Test
	public void trackUser() {
		Locale.setDefault(Locale.US);

		VisitedLocation visitedLocation = tourGuideService.trackUserLocationSync(user1);
		tourGuideService.tracker.stopTracking();

		assertEquals(user1.getUserId(), visitedLocation.userId);
	}

	@Test
	public void getFiveMostNearestAttractions() {
		Locale.setDefault(Locale.US);
		List<AttractionDTO> attractions = tourGuideService.getFiveMostNearestAttractions(user1);
		tourGuideService.tracker.stopTracking();
		assertEquals(5, attractions.size());
	}

	@Test
	public void getTripDeals() {
		List<Provider> providers = tourGuideService.getTripDeals(user1);
		tourGuideService.tracker.stopTracking();
		assertEquals(5, providers.size());
	}
}
