package tourGuide.service;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.UserRepository;
import tourGuide.repository.UserRepositoryStub;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.model.User;
import tourGuide.model.UserReward;

public class TestRewardsService {

	UserRepository repository = new UserRepositoryStub();
	GpsUtil gpsUtil = new GpsUtil();
	RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
	TourGuideService tourGuideService = new TourGuideService(repository, gpsUtil, rewardsService);

	@Test
	public void userGetRewards() {
		Locale.setDefault(Locale.US);
		InternalTestHelper.setInternalUserNumber(0);
		Attraction attraction = gpsUtil.getAttractions().get(0);
		User user1 = new User(
				UUID.randomUUID(),
				"jon",
				"000",
				"jon@tourGuide.com");
		user1.addToVisitedLocations(
				new VisitedLocation(user1.getUserId(), attraction, new Date()));

		tourGuideService.trackUserLocationSync(user1);
		List<UserReward> userRewards = user1.getUserRewards();
		tourGuideService.tracker.stopTracking();
		assertEquals(1, userRewards.size());
	}
	
	@Test
	public void isWithinAttractionProximity() {
		Attraction attraction = gpsUtil.getAttractions().get(0);

		assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
	}

	@Test
	public void nearAllAttractions() {
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);
		InternalTestHelper.setInternalUserNumber(0);
		User user = tourGuideService.getAllUsers().get(0);
		rewardsService.calculateRewards(user);

		List<UserReward> userRewards =
				tourGuideService.getUserRewards(
						user);
		System.out.println(userRewards);

		assertEquals(gpsUtil.getAttractions().size(), userRewards.size());
	}
	
}
