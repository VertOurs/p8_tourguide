package tourGuide;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;

import org.junit.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;

import tourGuide.repository.UserRepositoryStub;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.model.User;


@SpringBootTest
public class TestPerformance {
	



	@Before
	public void setUp() throws Exception {
		//InternalTestHelper.setInternalUserNumber(100000);
		Locale.setDefault(Locale.US);
	}

	@Test
	public void highVolumeTrackLocation() {
		UserRepositoryStub repository = new UserRepositoryStub();
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(100000);
		TourGuideService tourGuideService = new TourGuideService(repository, gpsUtil, rewardsService);
		List<User> allUsers = tourGuideService.getAllUsers();
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		for(User user : allUsers) {
			tourGuideService.trackUserLocation(user);
		}


		for(User user : allUsers) {

			User user1 = tourGuideService.getUser(user.getUserName());
			while (user1.getVisitedLocations().size() < 4) {
				try {
					TimeUnit.SECONDS.sleep(10);
				} catch (InterruptedException var2) {
				}
			}

		}


		allUsers.forEach(user -> user.getVisitedLocations());
		stopWatch.stop();
		tourGuideService.tracker.stopTracking();

		System.out.println("highVolumeTrackLocation: Time Elapsed: "
				+ TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())
				+ " seconds.");

		assertTrue(TimeUnit.MINUTES.toSeconds(15)
				>= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}


	@Test
	public void highVolumeGetRewards() {
		UserRepositoryStub repository = new UserRepositoryStub();
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(100000);
		TourGuideService tourGuideService = new TourGuideService(repository, gpsUtil, rewardsService);

		Attraction attraction = gpsUtil.getAttractions().get(0);
		List<User> allUsers = tourGuideService.getAllUsers();
		allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		for(User user : allUsers) {
			tourGuideService.trackUserLocation(user);
		}
		for(User user : allUsers) {
			while (user.getUserRewards().size() == 0) {
				try {
					TimeUnit.SECONDS.sleep(10);
				} catch (InterruptedException var2) {
				}
			}
			assertTrue(user.getUserRewards().size() > 0);
		}
		stopWatch.stop();
		tourGuideService.tracker.stopTracking();
		System.out.println("highVolumeGetRewards: Time Elapsed: "
				+ TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())
				+ " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(20)
				>= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}
}
