# Application TourGuide
Travel application whose performance tests were optimized as part of the OpenClassrooms Project 8
## Technical specifications
* java 8
* Gradle 4.8.1
* Spring Boot 2.1.6

## Endpoints
* **GET** `` http://localhost:8080/``
* **GET** `` http://localhost:8080/getLocation``
* **GET** `` http://localhost:8080/getNearbyAttractions``
* **GET** `` http://localhost:8080/getRewards``
* **GET** `` http://localhost:8080/getAllCurrentLocations``
* **GET** `` http://localhost:8080/getTripDeals``
* **PUT** ``http://localhost:8080/updateUserPreferences``
 